#include "TMP100.h"
#include "ArduinoSerial.h"
#include "TMP100Defines.h"
#include <stddef.h>

TMP100::TMP100(uint8_t resolution,
               uint8_t address)
{
   _i2c = new ArduinoSerial(address);
   _lastConvertion = 0.0f;
   setResolution(resolution);
}

TMP100::~TMP100()
{
   delete _i2c;
   _i2c = NULL;
}

float TMP100::getTemperature()
{
   return _lastConvertion;
}

uint16_t TMP100::convertionTime()
{
   uint16_t convTime;
   switch(_resolution)
   {
      case 9:
         convTime = CONVERTION_TIME_9BITS_MS;
         break;
      case 10:
         convTime = CONVERTION_TIME_10BITS_MS;
         break;
      case 11:
         convTime = CONVERTION_TIME_11BITS_MS;
         break;
      case 12:
         convTime = CONVERTION_TIME_12BITS_MS;
         break;
      default:
         convTime = CONVERTION_TIME_UNKNOWN;
         break;
   }
   return convTime;
}

uint8_t TMP100::getModifiedResolution(uint8_t res)
{
   uint8_t ret_val;
   // Select config register
   _i2c->sendByte(POINTER_ADDRESS_CONFIG);
   // Retrieve config register
   ret_val = _i2c->receiveByte();
   // Modify resolution in config register
   return (clearResolution(res) | convertToResolution(res));

}
int TMP100::setResolution(uint8_t res)
{
   if (res < 9 || res > 12) return -1;
   _resolution = res;

   uint8_t resConfigArray[] = {
                  POINTER_ADDRESS_CONFIG,          //Select CONFIG register
                  getModifiedResolution(res)};     //Config register with
                                                   //new resolution

   _i2c->sendArray(resConfigArray, 2);
   return 0;
}

int TMP100::sensorPeriodicTask()
{
   // Read sensor
   // Set Pointer Register to read temperature.
   _i2c->sendByte(POINTER_ADDRESS_READ);
   // Read sensor
   uint16_t tempSum = (_i2c->receiveWord()) >> 4;
   // Affect reading by an scale factor.
   _lastConvertion = tempSum*0.0625;
   return 0;
}
