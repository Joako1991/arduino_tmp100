#include "Serial.h"

SerialComunication::SerialComunication(uint8_t address,
                                       uint32_t baudRate) :
                                _devAddress(address),
                                _baudRate(baudRate)
{
}

void SerialComunication::sendByte(uint8_t data)
{
   (void)data;
}

void SerialComunication::sendArray(uint8_t *buf, uint32_t length)
{
   (void)buf;
   (void)length;
}

uint8_t SerialComunication::receiveByte()
{
   return 0;
}

uint16_t SerialComunication::receiveWord()
{
   return 0;
}

void SerialComunication::setAddress(uint8_t addr)
{
   _devAddress = addr;
}

void SerialComunication::setBaudRate(uint32_t baudR)
{
   _baudRate = baudR;
}

uint8_t SerialComunication::getAddress()
{
   return _devAddress;
}

uint32_t SerialComunication::getBaudRate()
{
   return _baudRate;
}
