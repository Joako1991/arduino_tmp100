#include <SimpleTimer.h>

//This Example will make work an tempeture sensor TMP100 from Texas Instruments
// I2C configuration:
// SDA --> A4
// SCL --> A5

#include <Wire.h>
#include "TMP100.h"

SensorTemperatura *temp;
SimpleTimer timer1;
SimpleTimer timer2;

void repeatMe()
{
   temp->sensorPeriodicTask();

}
void printTemp()
{
   float temperatura = temp->getTemperature();
   Serial.println("\nTemperatura Leida: ");
   Serial.print(temperatura);
}

void setup() {
   // put your setup code here, to run once:
   Serial.begin(9600);
   temp = new TMP100(SENSOR_RESOLUTION, SENSOR_ID);
   timer1.setInterval(100, repeatMe);
   timer2.setInterval(500, printTemp);
}

void loop() {
   // put your main code here, to run repeatedly:
   timer1.run();
   timer2.run();
}
