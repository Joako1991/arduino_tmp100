#ifndef _TMP100_H_
#define _TMP100_H_

/*
* Sensor as Slave Receiver:
* Data Structure:
* 1st Byte             -->  Slave address with RW bit as low(8th bit)   Wait for acknowledge
* 2nd byte             -->  Pointer Register                            Wait for acknowledge
* 3rd byte to Nth byte -->  Data to be written at Pointer register      Wait for acknowledge of each sent byte
* last byte            -->  Generate STOP / START condition
*/

/*
* To change Pointer Register, first we send address with RW bit as low,
* then we send the pointer value, and then send again address but with RW
* bit as HIGH
*/

/*
* Sensor as Slave Transmitter:
* Data Structure:
* 1st Byte             -->  Slave addrees with RW bit as high(8th bit)        Wait for acknowledge
* The next byte is sent by the slave. It is the MSB of the data at the register pointed by Pointer Register
* Master should send an acknowledge to the slave when received
* The next byte is sent by the slave. It is the LSB of the data at the register pointed by Pointer Register
* Master should send an acknowledge to the slave when received
* last byte            -->  Send Not Acknowledge on reception or Generate STOP / START condition
*/

#include "SensorTemperatura.h"
#include "Serial.h"
#define SENSOR_RESOLUTION     12
#define SENSOR_ID           0x48


class TMP100 : public SensorTemperatura
{
public:

   /*
      @brief TMP100: Constructor. It will instantiate a i2c communication
                     object, set sensor resolution, and clear lastConvertion
                     member.
      @param resolution: Sensor resolution
      @param addr: Sensor device address
   */
   TMP100(uint8_t resolution, uint8_t addr);

   ~TMP100();

   /*
      @brief getTemperature: Get last received temperature.
      @return temperature in Celcious
   */
   virtual float getTemperature();

   /*
      @brief sensorPeriodicTask: Periodic task to be execute periodically
                                 to refresh stored temperature.
      @return 0 if no error occurs
   */
   virtual int sensorPeriodicTask();

   /*
      @brief setResolution: Configure sensor resolution.
      @param res: Resolution to be set, expressed in bits.
                  It should be a value between 9 and 12.
      @return 0 if no error occurs, or -1 if res has an invalid value.
   */
   int setResolution(uint8_t res);

   /*
      @brief convertionTime: Get sensor convertion time. This value depends on
                             sensor resolution. The less bits you set, the
                             faster the sensor is.
      @return Sensor convertion time in MilliSeconds
   */
   uint16_t convertionTime();

private:

   /*
      @brief getModifiedResolution: Get and modify configuration register
                                    from sensor to set desired resolution.
      @param res: Resolution to be set.
      @return Config register with desired resolution to be set.
   */
   uint8_t getModifiedResolution(uint8_t res);

private:
   SerialComunication *_i2c;
   float _lastConvertion;
   uint16_t _resolution;
};

#endif
