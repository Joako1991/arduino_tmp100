#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <stdint.h>

typedef enum
{
   I2C_WRITE = 0x00,
   I2C_READ  = 0x80,
}I2C_ReadWrite_t;

class SerialComunication
{
public:
   /*
      @brief SerialComunication: Constructor.
      @param address: Serial device address
      @param baudRate: Transmition BaudRate
   */
   SerialComunication(uint8_t address, uint32_t baudRate);

   /*
      @brief setAddress: Change device address
      @param address: Serial device address
   */
   void setAddress(uint8_t addr);

   /*
      @brief getAddress: Get device address
      @return: Serial device address
   */
   uint8_t getAddress();

   /*
      @brief getBaudRate: Get communication configured baudRate.
      @return: Baudrate
   */
   uint32_t getBaudRate();

   //Virtual Methods

   /*
      @brief setBaudRate: Set communication baudRate.
      @param baudR: Baudrate to be set.
   */
   virtual void setBaudRate(uint32_t baudR);

   /*
      @brief sendByte: Send one byte throught serial communication.
      @param data: byte to be sent
   */
   virtual void sendByte(uint8_t data);

   /*
      @brief receiveByte: Receive a byte throught serial communication.
      @return Byte received.
   */
   virtual uint8_t receiveByte();

   /*
      @brief receiveWord: Receive a word throught serial communication.
      @return Word read as two bytes. The first byte read is the MSB and
              the second one is the LSB.
   */
   virtual uint16_t receiveWord();

   /*
      @brief sendArray: Send a bytes array throught serial communication.
      @param buf: Pointer where data to be sent is.
      @param length: array length
   */
   virtual void sendArray(uint8_t *buf, uint32_t length);

//Members
private:
   uint8_t _devAddress;
   uint32_t _baudRate;
};

#endif
