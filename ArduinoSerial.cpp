#include "ArduinoSerial.h"
#include <Wire.h>

#define RW_MASK   0x7F

ArduinoSerial::ArduinoSerial(uint8_t address) :
                                 SerialComunication(address,0)
{
   Wire.begin(address);
}

void ArduinoSerial::sendByte(uint8_t data)
{
   startTransmition(I2C_WRITE);
   Wire.write(data);
   stopTransmition();
}

uint8_t ArduinoSerial::receiveByte()
{
  Wire.requestFrom(getAddress(),1);
  return (uint8_t)Wire.read();
}

uint16_t ArduinoSerial::receiveWord()
{
  Wire.requestFrom(getAddress(),2);
  uint8_t FirstByte = Wire.read();
  uint8_t SecondByte = Wire.read();
  return uint16_t(uint16_t(FirstByte)<<8 |
                  uint16_t(SecondByte));
}

void ArduinoSerial::sendArray(uint8_t *buf, uint32_t length)
{
   startTransmition(I2C_WRITE);
   for(unsigned i = 0; i < length; i++)
   {
      sendByte(buf[i]);
   }
   stopTransmition();
}

void ArduinoSerial::startTransmition(I2C_ReadWrite_t RWFlag)
{
   Wire.beginTransmission(RWFlag |
                          (RW_MASK & getAddress()));
}

void ArduinoSerial::stopTransmition()
{
   Wire.endTransmission();
}
