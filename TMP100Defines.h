#ifndef _TMP100_DEFINES_H_
#define _TMP100_DEFINES_H_

// Pointer registers values
#define POINTER_ADDRESS_READ         0x00
#define POINTER_ADDRESS_CONFIG       0x01
#define POINTER_ADDRESS_TEMP_MIN     0x02
#define POINTER_ADDRESS_TEMP_MAX     0x03

#define RESOLUTION_BITS                 2
// This offset is where resolution bits starts
#define RESOLUTION_OFFSET               5
// Sensor resolution bits mask
#define RESOLUTION_MASK         (((1<<RESOLUTION_BITS)-1)<<RESOLUTION_OFFSET)

#define convertToResolution(ResolutionBits)  \
                                    ((ResolutionBits-9)<<RESOLUTION_OFFSET)

// Clear resolution bits in Config register
#define clearResolution(reg)        (reg&(!RESOLUTION_MASK))


// These times have been taken from datasheet
#define CONVERTION_TIME_9BITS_MS       40
#define CONVERTION_TIME_10BITS_MS      80
#define CONVERTION_TIME_11BITS_MS     160
#define CONVERTION_TIME_12BITS_MS     320
#define CONVERTION_TIME_UNKNOWN         0

#endif
