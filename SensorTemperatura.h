#ifndef _SENSOR_TEMPERATURA_H_
#define _SENSOR_TEMPERATURA_H_

class SensorTemperatura
{

public:
   SensorTemperatura();
   virtual int configSensor();
   virtual float getTemperature();
   virtual int sensorPeriodicTask();

private:
   float lastConvertion;
};

#endif
