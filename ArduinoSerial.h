#ifndef _ARDUINO_SERIAL_H_
#define _ARDUINO_SERIAL_H_

#include "Serial.h"

class ArduinoSerial : public SerialComunication
{
public:
   /*
      @brief ArduinoSerial: Constructor. Init parent and join to address for
                            I2C communication
      @param address: I2C device to connect address
   */
   ArduinoSerial(uint8_t address);

   /*
      @brief sendByte: Send one byte throught I2C.
      @param data: byte to be sent
   */
   virtual void sendByte(uint8_t data);

   /*
      @brief receiveByte: Request and read a byte received. Before sending, a
                          start transmition condition is generated, and a stop
                          condition is generated after sending.
      @return Byte read
   */
   virtual uint8_t receiveByte();

   /*
      @brief receiveWord: Request word and read it when received.
      @return Word read as two bytes. The first byte read is the MSB and
              the second one is the LSB.
   */
   virtual uint16_t receiveWord();

   /*
      @brief sendArray: Send a bytes array throught I2C.
      @param buf: Pointer where data to be sent is.
      @param length: array length
   */
   virtual void sendArray(uint8_t *buf, uint32_t length);

   /*
      @brief startTransmition: Generate a start transmition condition.
      @param RWFlag: Read Write flag.
   */
   void startTransmition(I2C_ReadWrite_t RWFlag);

   /*
      @brief stopTransmition: Generate a stop transmition condition.
   */
   void stopTransmition();
};
#endif
